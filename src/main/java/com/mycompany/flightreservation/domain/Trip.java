/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

import java.time.ZonedDateTime;

/**
 *
 * @author Room107
 */
public class Trip {
    
    private Airport fromAirPort;
    private Airport toAirPort;
    private byte noOfPassengers;
    private ZonedDateTime departureDateTime;
    private ZonedDateTime eturnDateTime;
    private boolean directFlight;
    private TravelClass travelClass;
    
}
